import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore/lite'
import {getAuth} from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCoEJ4ho7_RfZ3CcnoMVxT_OgUHwZk5D3Y",
    authDomain: "andry-b06d9.firebaseapp.com",
    projectId: "andry-b06d9",
    storageBucket: "andry-b06d9.appspot.com",
    messagingSenderId: "762552493840",
    appId: "1:762552493840:web:9799d13cc4b56d23f96795"
  };
  



// eslint-disable-next-line no-unused-vars
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
export const auth = getAuth(app);
export default db