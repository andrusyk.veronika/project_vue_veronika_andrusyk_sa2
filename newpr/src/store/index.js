import { createStore } from 'vuex'
import students from "./modules/students";
import auth from "./modules/auth";
import lists from "./modules/lists";
import slists from "./modules/slists";
import plists from "./modules/plists";
import pralkaLs from "./modules/pralkaLs";

export default createStore({
  namespaced: true,
  modules: {
    students,
    auth,
    lists,
    slists,
    plists,
    pralkaLs
  }
})
